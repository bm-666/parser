from selenium.webdriver.common.by import By



class Locators:
    block_raiting: tuple = (By.CLASS_NAME, "business-reviews-card-view__title")
    class_css_flip: tuple = (By.CLASS_NAME, 'business-reviews-card-view__ranking')
    locator_comments: tuple = (By.CLASS_NAME, 'business-reviews-card-view__review')
    css_class_raiting: tuple = (By.CLASS_NAME, 'business-rating-badge-view__rating-text')
    css_class_header: tuple = (By.CLASS_NAME, 'business-reviews-card-view__ranking')
    count_comments: tuple = (By.CLASS_NAME, 'business-reviews-card-view__title')
    section_comments: tuple = (By.CLASS_NAME, '_name_reviews')
    locator_container: tuple = (By.CLASS_NAME, 'business-reviews-card-view__reviews-container')
    locator_type_sorts: tuple = (By.CLASS_NAME, 'rating-ranking-view')
    locator_sort_by_new: tuple = (By.CLASS_NAME, 'rating-ranking-view__popup-line')
    # JavaSript elements
    js_code_select_views_new = "return document.querySelectorAll('.rating-ranking-view__popup-line')[1].click();"
    js_code_click_sort_views = 'return document.querySelectorAll(".business-reviews-card-view__ranking")[1].querySelector(".flip-icon").click();'