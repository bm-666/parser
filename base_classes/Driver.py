from selenium.webdriver import chrome
from selenium.webdriver.chrome import options
from selenium.webdriver.common import desired_capabilities, keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from seleniumwire import webdriver
from fake_user_agent import user_agent
from loguru import logger
import time


class Driver:
    #width =
    #hight =

    def __init__(self):
        self.proxy_server = "iparchitect_23863_21_07_23:BdfnSH3QhE9hAa3zQ5@188.143.169.27:30151"
        self._driver = self.__init_driver()

    def get_driver(self):
        return self._driver

    def __init_driver(self):
        options = webdriver.ChromeOptions()
        ua = user_agent()
        print(f'USER AGENT: {ua}')

        #options.add_experimental_option("prefs", {
        #    "profile.default_content_setting_values.notifications": 2,
        #})
        options.add_argument("--disable-blink-features=AutomationControlled")
        options.add_argument("--no-sandbox")
        #options.add_argument("--headless")
        ua = 'Mozilla/5.0 (iPad; CPU OS 16_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.5 YaBrowser/23.5.5.336 Mobile/15E148 Safari/605.1'
        options.add_argument(f'user-agent={ua}')
        options.add_argument("--disable-dev-shm-usage")
        #options.add_experimental_option("excludeSwitches", ["enable-automation"])
        #options.add_experimental_option("useAutomationExtension", False)
        #options.add_argument(f"proxy-server={self.proxy_server}")
        # options.add_argument(f"user-data-dir={self.__cookies_path}")

        driver: webdriver = webdriver.Chrome(
            options=options
        )
        driver.set_window_size(768, 1366)
        return driver


class PageElement:
    timeout: int = 20

    def load_element(self, locator):
        # Ожидаем зарузки элемента на странице
        element: None = None
        try:
            element = WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located(locator))
        except Exception as error:
            logger.error(f"Ошибка в переданных параметрах: {error.args}, locator:{locator}")

        return element

    def load_elements(self, locator):
        # Ожидаем зарузки элементов
        elements: None = None
        try:
            elements = WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_all_elements_located(locator))
        except Exception as error:
            logger.error(f"Ошибка в переданных параметрах {error.args}, {locator}")
        return elements




    def scroll_element_page(self, count_comments: int, locator: tuple) -> list:
        # Прокручивание динамически загружаемых коментариев
        logger.info('start scroll')
        comments: list = []
        result: set = set()
        seconds: float = 1.5
        load = self.load_element(locator)
        logger.info(load)
        if load:
            '''if count_comments < 50:
                cmt = 50
            else:
                cmt = count_comments'''
            coord = 0
            cmt = 50 if count_comments <= 50 else count_comments
            while len(result) < cmt:
                coord = coord + 500
                print(count_comments)
                logger.info(f"Name: document.querySelector('.scroll__container').scrollTo(0, {coord});")
                self.driver.execute_script(
                    f"document.querySelector('.scroll__container').scrollTo(0, {coord});")
                if len(result) > 500:
                    seconds = 3.0
                time.sleep(seconds)
                elements = {i for i in self.load_elements(locator)}
                result = result.union(elements)
                logger.debug(f'LEN ELEMENTS: {len(elements)}')
            comments = list(result)

        return comments
