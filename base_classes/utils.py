from datetime import datetime as dt


ALL_MONTH = {'января': '01', 'февраля': '02', 'марта': '03', 'апреля': '04', 'мая': '05', 'июня': '06',
                 'июля': '07', 'августа': '08', 'сентября': '09', 'октября': '10', 'ноября': '11', 'декабря': '12'}

def date_convert(args:str):
    # Приведение даты к формату DD.MM.YYYY
    new_date: str = ''
    try:
        list_args = args.split()
        month = ALL_MONTH
        list_args[1] = month[list_args[1]]
        if len(list_args) > 2:
            new_date = '.'.join(list_args)
        elif len(list_args) == 2:
            list_args.append(str(dt.now().year))
            new_date = '.'.join(list_args)

    except IndexError:
        return
    return new_date
