import time

from utils.Driver import Driver, PageElement
from utils.Locators import Locators
from datetime import date, timedelta, datetime
from loguru import logger
from multiprocessing.pool import Pool

class BaseParser(Driver, Locators, PageElement):
    pass

class Parser(BaseParser):
    def __init__(self, url: str):
        super().__init__()
        self.url: str = url
        self.driver = self.get_driver()
    def run(self):

        logger.info(f'start parsing url {self.url}')
        self.driver.get(self.url)
        time.sleep(20)
        self.load_element(self.block_raiting)
        element_raiting = self.load_element(self.css_class_raiting)
        self.driver.execute_script("return document.querySelector('._name_reviews').click();")
        element_count_comments = self.load_element(self.count_comments)
        raiting = element_raiting.text.replace(',', '.') if element_raiting is not None else '0.0'
        count_comments = int(element_count_comments.text.split()[0])
        statistics : dict = {
            'raiting': raiting,
            'count_comments': count_comments,
            'date_parse': date.today()
        }
        self.driver.execute_script("document.querySelector('.scroll__container').scrollTo(0, 1000);")

        section_reviews = self.load_element(self.section_comments)
        if section_reviews:
            section_reviews.click()
            time.sleep(15)
            container = self.load_element(self.locator_container)
            self.load_elements(self.locator_type_sorts)[1].click()
            self.load_elements(self.locator_sort_by_new)[1].click()

            if container:
                comments = self.scroll_element_page(count_comments=count_comments, locator=self.locator_comments)
                print(comments)


def start_parser(url: str) -> dict:
    try:
        parser = Parser(url)
        result: dict = parser.run()
        return result
    except Exception as error:
        logger.critical(error)
    finally:
        parser.driver.close()

if __name__ == '__main__':
    try:
        #urls: tuple = ('https://yandex.ru/profile/86050520434',)
        urls = ('https://2ip.online/', 'https://whatismyipaddress.com/')
        #'https://yandex.ru/profile/209166833846',
        with Pool(processes=2) as pool:
            results: list = pool.map(start_parser, urls)

            print(results)

    except Exception as error:
        logger.error(f'error: {error.args}')
